import ky from 'ky';

export default class RequestApi {
    constructor(server_url, access_key, refresh_key) {
        this.server_url = server_url;
        this.access_key = access_key;
        this.refresh_key = refresh_key;
    }


    async getToken(username, password) {
        let result = await ky.post(this.server_url + "/api/token/", {
            json: {
                "username": username, "password": password
            }
        }).json();
        return result;
    }

    async refreshTokens() {
        let result = await ky.post(this.server_url + "/api/token/refresh/", {
            json: {}
        }).json();
        this.refresh_key = result["refresh"];
        this.access_key = result["access"]
        return true;
    }

    async apiMethod(method, path, data, auth) {
        const url = this.server_url + path;
        let ky_token = undefined
        if (auth) {
            ky_token = ky.create({
                headers: {
                    "Authorization": `Bearer ${this.access_key}`
                }
            });
        } else {
            ky_token = ky.create({});
        }
        let result = undefined;
        switch (method) {
            case 'POST':
                result = await ky_token.post(url, {json: data})
                break;
            case 'GET':
                result = await ky_token.get(url)
                break;
            case "PUT":
                result = await ky_token.put(url, {json: data})
                break;
            case "DELETE":
                result = await ky_token.delete(url, {json: data})
                break;
        }
        let obj_result = result.json();
        if (obj_result["code"] === "token_not_valid") {
            if (obj_result["messages"]["message"] === "Token is invalid or expired") {
                result = await this.refreshTokens();
                return await this.apiMethod(method, path, data);
            }
        } else {
            return obj_result;
        }

    }


}