import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue';
import News from '../views/NewsList.vue';
import NewsPage from "@/views/NewsPage";
import NewsEditor from "@/views/NewsEditor";

Vue.use(VueRouter)

const routes = [

  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {

    path: '/news/editor/:id?',
    name: 'NewsEditor',
    component: NewsEditor,
  },
  {
    path: '/news/:id',
    name: 'NewsPage',
    component: NewsPage,
  },
  {
    path: '/news/page/:page',
    name: 'news',
    component: News
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path : "/login",
    name : "Login",
    component : () => import('../views/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
