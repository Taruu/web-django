import Vue from 'vue'
import Vuex from 'vuex'
import RequestApi from "@/functions/RequestApi";
import router from '../router'

Vue.use(Vuex)
const server_url = "http://127.0.0.1:8000"
export default new Vuex.Store({
    state: {
        api: new RequestApi(server_url, localStorage.access_token, localStorage.refresh_token),
        username: localStorage.username,
        news_cache: {},
        pages: {},
        now_news_text: "",
        max_page: 0,
        news_editor: ""

    },
    getters: {
        GET_NEWS_FROM_CACHE: (state) => (id) => {
            return {
                ...state.news_cache[id]
            };
        },
        GET_PAGE: (state) => (page) => {
            console.log("PAGEes", state.pages)
            let list_id_news = state.pages[page];
            let result = [];
            if (list_id_news.length > 0) {
                list_id_news.forEach(id => {
                    result.push({"id": id, ...state.news_cache[id]});
                });
                return result;
            }
        }
    },
    mutations: {
        SET_MAX_PAGE(state, payload) {
            state.max_page = payload.max_page;
        },
        ADD_NEWS(state, payload) {
            state.news_cache[payload.id] = {
                "name": payload.name,
                "owner": payload.owner,
                "content": payload.content,
                "updated_at": new Date(payload.updated_at)
            }
        },
        REMOVE_NEWS(state, payload) {
            delete state.news_cache[payload.id];
        },
        ADD_PAGE(state, payload) {
            state.pages[payload.page] = payload.now_page.map(news_obj => {
                return news_obj.id
            })

            for (let news of payload.now_page) {
                state.news_cache[news.id] = {
                    "name": news.name,
                    "owner": news.owner,
                    "content": news.content,
                    "updated_at": new Date(news.updated_at)
                }
            }
        },
        LOGIN(state, payload) {
            state.username = payload.username;
            state.api = new RequestApi(server_url,
                payload.access_token, payload.refresh_token);
            localStorage.access_token = payload.access_token;
            localStorage.refresh_token = payload.refresh_token;
            localStorage.username = payload.username;
            // this.localStorage.setItem("username", state.username)
            router.push("/")
        },
        LOGOUT(state) {
            state.username = "";
            localStorage.username = "";
            state.api = new RequestApi(server_url);
            localStorage.access_token = "";
            localStorage.refresh_token = "";
            router.push("/")
        },
        SAVE_NEWS(state, payload) {
            state.news_editor = payload.news_editor;
        }

    },
    actions: {
        LOGIN({commit, state}, payload) {
            state.api.getToken(payload.username, payload.password).then(result => {
                if (result["refresh"] && result["access"]) {
                    commit("LOGIN", {
                        username: payload.username,
                        access_token: result.access,
                        refresh_token: result.refresh
                    })
                }
            })
        },
        async DOWNLOAD_PAGE({commit, state}, payload) {
            if (!Object.keys(state.pages).includes(payload.page)) {
                return new Promise((resolve) => {
                    state.api.apiMethod("GET", `/api/news/${payload.page}`).then(result => {
                        result["page"] = payload.page;
                        commit("ADD_PAGE", result);
                        commit("SET_MAX_PAGE", result);
                        resolve()
                    })
                })

            }

        },
        async GET_NEWS({commit, state}, payload) {
            if (!Object.keys(state.news_cache).includes(payload.news_id) || payload.update) {
                return new Promise((resolve) => {
                    state.api.apiMethod("POST", `/api/news/0/`, {"news_id": Number(payload.news_id)}).then(result => {
                        commit("ADD_NEWS", result)
                        resolve()
                    })
                })
            }
        },
        async ADD_NEWS({state}, payload) {
            console.log("add news")
            if (payload.name && payload.content) {
                return new Promise((resolve) => {
                    state.api.apiMethod("POST", `/api/news/edit/`, {
                        "name": payload.name,
                        "content": payload.content
                    }, true).then(result => {
                        router.push(`/news/${result.id}`)
                        resolve()
                    })
                })
            }
        },
        async UPDATE_NEWS({commit, state}, payload) {
            console.log("update news")
            if (payload.name && payload.content && payload.news_id) {
                return new Promise((resolve) => {
                    console.log(payload.content)
                    state.api.apiMethod("PUT", `/api/news/edit/`, {
                        "news_id": payload.news_id,
                        "name": payload.name,
                        "content": payload.content
                    }, true).then(result => {
                        commit("ADD_NEWS", result)
                        router.push(`/news/${result.id}`)
                        resolve()
                    })
                })
            }
        },
        async REMOVE_NEWS({commit, state}, payload) {
            if (payload.id) {
                return new Promise((resolve) => {
                    state.api.apiMethod("DELETE", `/api/news/edit/`, {
                        "news_id": payload.id
                    }, true).then(() => {

                        router.push(`/news/page/0`)
                        commit("REMOVE_NEWS", payload.id)
                        resolve()
                    })
                })
            }
        },
        async FEEDBACK({state}, payload) {
            await state.api.apiMethod("POST", "/api/feedback/", {
                email : payload.email,
                name : payload.name,
                feedback : payload.feedback
            })
        }

    },
    modules: {}
})
